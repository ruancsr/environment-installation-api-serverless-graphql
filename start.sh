# INSTALLATION COMMANDS
# (1) chmod +x start.sh
# (2) ./start.sh

NAME_PROJECT=api-serverless-graphql
VERSION_MYSQL=8.0.26

echo "Downloading image MySQL"
docker pull mysql:${VERSION_MYSQL}

echo "Downloading image Redis"
docker pull redis

echo "Downloading project"
rm -rf ${NAME_PROJECT}
git clone git@gitlab.com:ruancsr/${NAME_PROJECT}.git

cd ./${NAME_PROJECT}/serverless
npm install

docker-compose down
docker-compose up -d